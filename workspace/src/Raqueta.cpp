// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	x1 = x1 + velocidad.x * t;//Movimiento de la primera raqueta en eje x
	y1 = y1 + velocidad.y * t;//Movimiento de la primera raqueta en eje y
	x2 = x2 + velocidad.x * t;//Movimiento de la segunda raqueta en eje x
	y2 = y2 + velocidad.y * t; //Movimientos de la segunda raqueta en eje y
}
