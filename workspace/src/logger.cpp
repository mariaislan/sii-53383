//Autora: Maria Islán
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


int main(int argc, char **argv)
{
	int n_read;
	char buffer[40];
	if(mkfifo("/tmp/Tuberia", 0777)!=0) //Creo la tubería
		printf ("Hubo un problema al crear la tuberia pipe\n");

	int mipipe=open("/tmp/Tuberia", O_RDONLY); //Abro la tubería en modo lectura
        if(mipipe<0)
        {
                perror("open");
                return 1;
        }

	while((n_read=read(mipipe, buffer, 40))>0) //Mientras haya algo para leer
	{
		printf("%s", buffer); //Imprime lo que lee por la salida estándar
	}
	if(n_read<0) 
	{
                perror("read");
                return 1;
        }

	close(mipipe); //Cierro la tubería

	int ret = unlink("/tmp/Tuberia");  //Destruyo la tubería
	if(ret<0)
	{
                perror("unlink");
                return 1;
        }

	return 0;
}

